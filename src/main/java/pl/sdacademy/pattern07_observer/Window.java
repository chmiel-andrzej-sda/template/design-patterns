package pl.sdacademy.pattern07_observer;

public class Window {
    private boolean open;

    public void open() {
        open = true;
    }

    public void close() {
        open = false;
    }
}
