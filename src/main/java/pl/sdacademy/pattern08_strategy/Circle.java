package pl.sdacademy.pattern08_strategy;

public class Circle {
    private final int radius;
    private int color = 0;

    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Circle[r=" + radius + ", c=" + color + "]";
    }
}
