package pl.sdacademy.pattern06_command;

public class Beetle {
    private int x = 0;
    private int y = 0;
    private char symbol = '>';

    public void move(int x, int y) {
        this.x += x;
        this.y += y;
    }

    public char getSymbol() {
        return symbol;
    }

    public void transform(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "Beetle[symbol='" + symbol + "', x=" + x + ", y=" + y + "]";
    }
}
