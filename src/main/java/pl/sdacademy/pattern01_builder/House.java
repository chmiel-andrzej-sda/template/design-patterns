package pl.sdacademy.pattern01_builder;

public class House {
    private int floors;
    private boolean garage;
    private int rooms;
    private boolean garden;
    private boolean cellar;
    private double surface;

    public House(int floors, boolean garage, int rooms, boolean garden, boolean cellar, double surface) {
        this.floors = floors;
        this.garage = garage;
        this.rooms = rooms;
        this.garden = garden;
        this.cellar = cellar;
        this.surface = surface;
    }
}
