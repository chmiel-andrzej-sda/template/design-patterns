package pl.sdacademy.pattern05_chainofresponsibility;

import pl.sdacademy.pattern05_chainofresponsibility.shape.Shape;
import pl.sdacademy.pattern05_chainofresponsibility.shape.Square;
import pl.sdacademy.pattern05_chainofresponsibility.shape.Trapeze;
import pl.sdacademy.pattern05_chainofresponsibility.shape.Triangle;

public class ChainOfResponsibility {
	public static void main(String[] args) {
		Shape shape = new Triangle();
		detect(shape);
	}

	private static void detect(Shape shape) {
		if (shape instanceof Triangle) {
			System.out.println("it's triangle!");
			System.out.println(((Triangle) shape).countArea(1, 3));
		}

		if (shape instanceof Square) {
			System.out.println("It's a square!");
			System.out.println(((Square)shape).countArea(12));

		}

		if (shape instanceof Trapeze) {
			System.out.println("It's a trapeze!");
			System.out.println(((Trapeze)shape).countArea(1, 2, 1));
		}
	}
}
