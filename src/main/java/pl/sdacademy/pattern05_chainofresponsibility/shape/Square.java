package pl.sdacademy.pattern05_chainofresponsibility.shape;

public class Square implements Shape {
	public double countArea(double a) {
		return a * a;
	}
}
