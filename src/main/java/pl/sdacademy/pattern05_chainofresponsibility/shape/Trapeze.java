package pl.sdacademy.pattern05_chainofresponsibility.shape;

public class Trapeze implements Shape {
	public double countArea(double a, double b, double h) {
		return (a + b) * h / 2;
	}
}
