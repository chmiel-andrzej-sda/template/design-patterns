package pl.sdacademy.pattern05_chainofresponsibility.shape;

public class Triangle implements Shape {
	public double countArea(double a, double h) {
		return a * h / 2;
	}
}
