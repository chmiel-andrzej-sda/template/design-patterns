package pl.sdacademy.pattern04_facade;

public class Valve {
	private boolean opened;

	public void open() {
		this.opened = true;
	}

	public void close() {
		this.opened = false;
	}

	public boolean isOpened() {
		return opened;
	}
}
