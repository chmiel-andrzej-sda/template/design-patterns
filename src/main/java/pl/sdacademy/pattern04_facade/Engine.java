package pl.sdacademy.pattern04_facade;

public class Engine {
	private int level;

	public void setLevel(int level) {
		if (level < 0) {
			level = 0;
		}
		this.level = level;
	}
}
