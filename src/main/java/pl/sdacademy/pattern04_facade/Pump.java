package pl.sdacademy.pattern04_facade;

public class Pump {
	private boolean working;

	public void turnOn() {
		working = true;
	}

	public void turnOff() {
		working = false;
	}

	public boolean isWorking() {
		return working;
	}
}
