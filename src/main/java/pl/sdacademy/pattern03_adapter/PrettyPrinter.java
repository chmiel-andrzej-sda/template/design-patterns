package pl.sdacademy.pattern03_adapter;

public class PrettyPrinter {
    private MyObject object;

    public PrettyPrinter(MyObject object) {
        this.object = object;
    }

    public void prettyPrint() {
        // object.printRow1();
        // object.printRow2();
        // object.printRow3();
    }
}
