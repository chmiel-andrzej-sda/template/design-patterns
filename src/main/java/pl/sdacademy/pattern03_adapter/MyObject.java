package pl.sdacademy.pattern03_adapter;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * DO NOT MODIFY THIS CLASS!!
 */
public class MyObject {
    private final List<Integer> list;
    private static final Random random = new Random();

    public MyObject() {
        list = Stream
                .generate(random::nextInt)
                .limit(30)
                .collect(Collectors.toList());
    }

    public List<Integer> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "MyObject{" +
                "list=" + list +
                '}';
    }
}
