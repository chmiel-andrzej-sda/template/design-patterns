package pl.sdacademy.pattern09_templatemethod;

import java.util.ArrayList;
import java.util.List;

public abstract class Action {
    public static final List<Action> actions = new ArrayList<>();

    public static int execute(int x) {
        for (Action action : actions) {
            x = action.doMagic(x);
        }
        return x;
    }

    protected Action() {
        actions.add(this);
    }

    public abstract int doMagic(int x);
}
