package pl.sdacademy.pattern09_templatemethod;

public class MagicalAction extends Action {
    @Override
    public int doMagic(int x) {
        return x + 1;
    }
}
