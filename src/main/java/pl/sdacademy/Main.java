package pl.sdacademy;

import pl.sdacademy.pattern01_builder.House;
import pl.sdacademy.pattern02_factorymethod.Door;
import pl.sdacademy.pattern03_adapter.MyObject;
import pl.sdacademy.pattern03_adapter.PrettyPrinter;
import pl.sdacademy.pattern04_facade.Engine;
import pl.sdacademy.pattern04_facade.Pump;
import pl.sdacademy.pattern04_facade.Valve;
import pl.sdacademy.pattern06_command.Beetle;
import pl.sdacademy.pattern07_observer.Window;
import pl.sdacademy.pattern08_strategy.Circle;
import pl.sdacademy.pattern09_templatemethod.Action;
import pl.sdacademy.pattern09_templatemethod.MagicalAction;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // 01. Builder
        final House h = new House(2, false, 4, true, true, 195.43);
        System.out.println(h);

        // 02. Factory method
        final Door door1 = new Door();
        door1.setColor("WHITE");
        door1.setLeft(true);
        door1.setMaxAngle(90);
        door1.setLock(false);
        door1.setWindow(true);

        final Door door2 = new Door();
        door2.setColor("BROWN");
        door2.setLeft(false);
        door2.setMaxAngle(135.5);
        door2.setLock(true);
        door2.setWindow(false);

        // 03. Adapter
        MyObject myObject = new MyObject();
        PrettyPrinter prettyPrinter = new PrettyPrinter(myObject);
        prettyPrinter.prettyPrint();

        // 04. Facade
        Pump pump = new Pump();
        Engine engine = new Engine();
        Valve valve = new Valve();
        //for opening
        pump.turnOn();
        engine.setLevel(10); // is it enough? :o
        valve.open();
        //for closing
        pump.turnOff();
        engine.setLevel(-1); //won't it crash?
        valve.close();

        // 06. Command
        beetle();

        // 07. Observer
        Window window = new Window();
        window.open();
        window.open();
        window.close();
        window.open();
        window.close();
        window.close();

        // 08. Strategy
        Circle circle = new Circle(12);
        circle.setColor(1);
        System.out.println(circle);

        // 09. Template Method
        new MagicalAction();
        System.out.println(Action.execute(10));

    }

    private static void beetle() {
        System.out.print("> ");
        Beetle beetle = new Beetle();

        Scanner scanner = new Scanner(System.in);

        String line;
        while ((line = scanner.nextLine()).length() != 0) {
            System.out.println("\"" + line + "\", " + line.length());
            String[] args = line.split(" ");
            switch (args[0].charAt(0)) {
                case 'm' -> beetle.move(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
                case 't' -> beetle.transform(args[1].charAt(0));
            }
            System.out.println(beetle);
            System.out.print("> ");
        }

    }
}
